/**
 * Created by Komo on 25/03/2015.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var UserSchema = new Schema({
    provider: String,
    oauthId: String,
    token: String,
    username: String,
    displayName: String,
    image: String,
    created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('User', UserSchema);