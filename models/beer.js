/**
 * Created by Komo on 25/03/2015.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var Review = require('./review');
var Promise = require('bluebird');

var BeerSchema = new Schema({
    _user: {type: Schema.Types.ObjectId, ref: 'User'},
    name: String,
    description: String,
    where: String,
    price: Number,
    average: {type: Number, default: 0},
    created: {type: Date, default: Date.now},
    _reviews: [{ type: Schema.Types.ObjectId, ref: 'Review'}]
});

//
BeerSchema.method('getAverage', function () {

    var beerId = this._id;
    return Review.aggregate([
        {$match: {_beer: {$eq: beerId}}},
        {$group: {_id: "_beer", average: {$avg: '$rating'}}}
    ]).exec();

});


module.exports = mongoose.model('Beer', BeerSchema);