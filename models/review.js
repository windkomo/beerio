/**
 * Created by Komo on 25/03/2015.
 */

var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var ReviewSchema = new Schema({
    _user: {type: Schema.Types.ObjectId, ref: 'User'},
    _beer: {type: Schema.Types.ObjectId, ref: 'Beer'},
    comment: String,
    rating: Number,
    created: {type: Date, default: Date.now}
});

module.exports = mongoose.model('Review', ReviewSchema);