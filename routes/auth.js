var express = require('express');
var router = express.Router();
var passport = require('passport');

//Route for twitte rauthentication
router.get('/auth/twitter',
    passport.authenticate('twitter'),
    function(req, res){
    });

//Route callback after authentication
router.get('/auth/twitter/callback',
    passport.authenticate('twitter', { failureRedirect: '/login' }),
    function(req, res) {
        res.redirect('/');
    });

//Logging out
router.get('/logout', function(req, res){
    req.logout();
    res.redirect('/');
});

module.exports = router;

