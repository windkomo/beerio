//Middleware
exports.requireLogin = function(req, res, next) {
    if (req.session.passport.user) {
        next();
    } else {
        res.redirect("/auth/twitter");
    }
};
