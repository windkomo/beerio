var express = require('express');
var router = express.Router();
var _ = require('lodash');
var Beer = require('../models/beer');
var Review = require('../models/review');
var session = require('./session');
var Promise = require('bluebird');

/* GET beers */
router.get('/', function (req, res, next) {

    var sorts = [
        {code: 'dateDesc', sort: '-created'},
        {code: 'dateAsc', sort: 'created'},
        {code: 'nameAsc', sort: 'name'},
        {code: 'nameDesc', sort: '-name'},
        {code: 'mostPopular', sort: '-average'},
        {code: 'lessPopular', sort: 'average'}
    ];

    var sort = req.query.sort;
    var sortQuery;
    sort ? sortQuery = _.result(_.findWhere(sorts, {code: sort}), 'sort') : null;

    Beer.find().populate('_user').sort(sortQuery).exec().then(function (beers) {
        res.json(beers);
    }, function (err) {
        if (err) return next(err);
    });
});

/* GET all reviews of a beer (beer by id) */
router.get('/:id/reviews', function (req, res, next) {
    var beerId = req.params.id;
    Review.find({_beer: beerId}).populate('_user').exec().then(function (reviews) {
        res.json(reviews);
    }, function (err) {
        if (err) return next(err);
    });
});


/* POST a new beer */
router.post('/', session.requireLogin, function (req, res, next) {
    var beer;
    var beerJSON = _.merge(req.body, {_user: req.session.passport.user});
    beer = new Beer(beerJSON);

    beer.save().then(function (post) {
        res.json(post);
    }, function (err) {
        if (err) return next(err);
    });
});

module.exports = router;
