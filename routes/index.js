var express = require('express');
var router = express.Router();
var session = require('./session');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {title: 'Beerio', user: req.user});
});

/* Check session */

router.get('/session',  function(req, res, next) {
        res.json(req.session.passport.user);
});

module.exports = router;
