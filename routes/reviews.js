var express = require('express');
var router = express.Router();
var _ = require('lodash');
var Review = require('../models/review');
var Beer = require('../models/beer');
var session = require('./session');

/* POST a new review */
router.post('/', session.requireLogin, function (req, res, next) {

    var review = new Review(req.body);
    review._user = req.session.passport.user;

    review.save().then(function (post) {
        Beer.findById(review._beer, function(err, beer)
        {
            //Compute new average
            beer.getAverage().then(function(result)
            {
                //Update beer : add review to beer.reviews, update average
                Beer.update({ _id: review._beer }, { $set: {average: result[0].average}, $addToSet: { _reviews: review._id } }, {}).exec().then(function()
                {
                    res.json(post);
                });
            });
        });

}, function (err) {
    if (err) {
        return next(err);
    }
});
});

module.exports = router;
