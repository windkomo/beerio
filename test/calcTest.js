/**
 * Created by Komo on 29/03/2015.
 */

var calculator = require('../public/js/calc');
var expect = require('chai').expect;

describe("calculator", function () {

    describe('calc', function () {

        it('should return 0 when no parameter is passed', function () {
            expect(calculator.calc().toString()).to.equal(0);
        });

        it('should add parameters between themselves and return the sum', function () {
            expect(calculator.calc(0).toString()).to.equal(0);
            expect(calculator.calc(1, 5).toString()).to.equal(6);
            expect(calculator.calc(4, 4, 4).toString()).to.equal(12);
            expect(calculator.calc(7, 8, 11, 1).toString()).to.equal(27);
            expect(calculator.calc(8, -1).toString()).to.equal(7);
        });

        it('should chain function multiply', function () {
            expect(calculator.calc(2)(3).toString()).to.equal(6);
            expect(calculator.calc(4)(2)(2).toString()).to.equal(16);
            expect(calculator.calc(0)(20)(42).toString()).to.equal(0);
        });

        it('should add parameters between themselves and chain function multiply', function () {
            expect(calculator.calc(2, 4)(11, -1).toString()).to.equal(60);
            expect(calculator.calc(2, 5, -7)(8, 1)(3).toString()).to.equal(0);
            expect(calculator.calc(4)(11)(-1)(7, 4)(2 * 8).toString()).to.equal(-7744);
        });

    });

});