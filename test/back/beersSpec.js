/**
 * Created by Komo on 29/03/2015.
 */

'use strict';
var request = require('superagent-bluebird-promise');
var bluebird = require('bluebird');
var _ = require('lodash');
var chai = require('chai'),
    should = chai.should(),
    expect = chai.expect(),
    sinon = require('sinon'),
    mongoose = require('mongoose');
var app = require('../../app.js').app;

require('../../models/beer');

var Beer = mongoose.model('Beer');
var ObjectId = mongoose.Types.ObjectId();

describe('beers API', function ()
{

    describe('GET beers', function () {

        it('should return the list of beers with user populated', function (done) {
            request.get('http://localhost:3000/beers').then(function (result) {
                _.forEach(result.body, function (beer) {
                    beer.should.have.property('_id');
                    beer.should.have.property('name');
                    beer.should.have.property('_user');
                    beer._user.should.have.property('_id');
                    beer._user.should.have.property('username');
                });
                done();
            });
        });

        //TODO: code to refactor : sort, always same shoulds...
        function isSortedDesc(arrayToCheck) {
            return _.every(arrayToCheck, function (value, index, array) {
                return index === 0 || String(array[index - 1]) >= String(value);
            });
        }

        function isSortedAsc(arrayToCheck) {
            return _.every(arrayToCheck, function (value, index, array) {
                return index === 0 || String(array[index - 1]) <= String(value);
            });
        }

        it('should sort by date descending', function (done) {
            request.get('http://localhost:3000/beers?sort=dateDesc').then(function (result) {

                var dates = _.pluck(result.body, 'created');
                isSortedDesc(dates).should.equal(true);
                done();
            });
        });

        it('should sort by date ascending', function (done) {
            request.get('http://localhost:3000/beers?sort=dateAsc').then(function (result) {
                var dates = _.pluck(result.body, 'created');
                isSortedAsc(dates).should.equal(true);
                done();
            });
        });

        it('should sort by name descending', function (done) {
            request.get('http://localhost:3000/beers?sort=nameDesc').then(function (result) {
                var names = _.pluck(result.body, 'name');
                isSortedDesc(names).should.equal(true);
                done();
            });
        });

        it('should sort by name ascending', function (done) {
            request.get('http://localhost:3000/beers?sort=nameAsc').then(function (result) {
                var names = _.pluck(result.body, 'name');
                isSortedAsc(names).should.equal(true);
                done();
            });
        });

        it('should sort by popularity : rating descending', function (done) {
            request.get('http://localhost:3000/beers?sort=mostPopular').then(function (result) {
                var ratings = _.pluck(result.body, 'rating');
                isSortedDesc(ratings).should.equal(true);
                done();
            });
        });

        it('should sort by popularity : rating ascending', function (done) {
            request.get('http://localhost:3000/beers?sort=lessPopular').then(function (result) {
                var ratings = _.pluck(result.body, 'rating');
                isSortedAsc(ratings).should.equal(true);
                done();
            });
        });
    });

    describe('GET beers/id/reviews', function ()
    {
        it('should return a beer and its reviews', function (done)
        {
            //Get beers with reviews
            Beer.find({_reviews: {$not: {$size: 0}}}).then(function (result)
            {
                var beersWithReviewsIds = [];
                _.forEach(result, function (beer)
                {
                    beersWithReviewsIds.push(beer._id);
                    beer.should.have.property('_reviews').with.length.of.at.least(1);
                });

                //Array of promises
                var promises = [];
                _.forEach(beersWithReviewsIds, function (beerWithReviewsId)
                {
                    promises.push(
                    request.get('http://localhost:3000/beers/' + beerWithReviewsId + '/reviews').then(function (result) {
                        _.forEach(result.body, function (review) {
                            review.should.have.property('comment');
                            review.should.have.property('rating');
                        });
                    }));
                });

                //When all promises are fufilled
                bluebird.all(promises).then(function() {
                    done();
                });


            });

        });

    });



    describe('POST beer', function ()
    {
        it('should add a new beer', function (done)
        {
            var newBeer = {
                _id: ObjectId,
                name: "Bière de test",
                description: "Excellent bière de test",
                where: "Pékin",
                price: 54.10};

            request.post('http://localhost:3000/beers').send(newBeer).then(function (result) {
                //console.log(result);
                done();
            });

        });

    });



});




