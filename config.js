/**
 * Created by Komo on 31/03/2015.
 */

module.exports = {
    db: {
        development: "mongodb://localhost:27017/beerio",
        test: "mongodb://localhost:27017/beeriotest"
    }
};