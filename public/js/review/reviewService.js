/**
 * Created by Komo on 26/03/2015.
 */

angular.module('beerio.services').factory('reviewService', ['Restangular', function(Restangular) {

    var reviewService = {};
    var REVIEWS = 'reviews';

    //Post a new review of a beer
    reviewService.postReview = function(review)
    {
        return Restangular.all(REVIEWS).post(review);
    };

    //Get reviews by beer
    reviewService.getBeerReviews = function(beerId)
    {
        return Restangular.one('beers', beerId).getList('reviews');
    };

    return reviewService;
}]);
