/**
 * Created by Komo on 26/03/2015.
 */

angular.module('beerio.services').factory('beerService', ['Restangular', function(Restangular) {

    var beerService = {};
    var BEERS = 'beers';

    //Get all beers
    beerService.getBeers = function(sort)
    {
        return Restangular.all(BEERS).getList({sort: sort});
    };

    //Add a new beer
    beerService.addBeer = function(beer)
    {
        return Restangular.all(BEERS).post(beer);
    };

    return beerService;
}]);
