/**
 * Created by Komo on 26/03/2015.
 */
angular.module('beerio.controllers').controller('beerController', ['$scope', 'beerService',
    function ($scope, beerService) {

        $scope.beers = {};

        //Add a new beer
        $scope.addNewBeer = function()
        {
            var beer = {
                name: $scope.name,
                description: $scope.description,
                where: $scope.where,
                price: $scope.price === null || angular.isUndefined($scope.price) ? 0 : $scope.price.replace(",", ".")
            };

            beerService.addBeer(beer).then(function(result) {
                $scope.getBeers();
                $scope.name = "";
                $scope.description = "";
                $scope.where = "";
                $scope.price = "";

                $scope.addBeer.$setPristine();
            });
        };

}]);
