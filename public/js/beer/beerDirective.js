/**
 * Created by Komo on 03/02/2015.
 */

angular.module('beerio.directives').directive('beers', ['reviewService', 'sessionService', 'beerService' ,function (reviewService, sessionService, beerService) {
    return {
        restrict: 'E',
        templateUrl: 'js/beer/beers.tpl.html',
        controller: ['$scope', '$element', '$attrs', function ($scope, $element, $attrs) {
        }],
        link: function (scope, elem, attrs) {
            scope.reviews = {};

            scope.sorts = [
                {code: 'dateDesc', label: 'Most recent'},
                {code: 'dateAsc', label: 'Oldest'},
                {code: 'nameAsc', label: 'Name ascending'},
                {code: 'nameDesc', label: 'Name descending'},
                {code: 'mostPopular', label: 'Most popular'},
                {code: 'lessPopular', label: 'Less popular'}
            ];

            scope.selectSort = function(sortChoice)
            {
                scope.getBeers(sortChoice);
            };

            //Get all beers
            scope.getBeers = function(sort)
            {
                beerService.getBeers(sort).then(function (result) {
                    console.log(result);
                    scope.beers = result;
                });
            };

            scope.getBeers();

            //TODO: move in parent controller ?
            scope.getSessionUser = function () {
                sessionService.getSessionUser().then(function (results) {
                    scope.user = results;
                });
            };

            scope.getSessionUser();

            //Get reviews of current beer
            scope.getReviews = function (beerId) {
                reviewService.getBeerReviews(beerId).then(function (results) {
                    scope.reviews = results;
                });
            };

            //Triggers on rating hover
            scope.hoveringOver = function (value) {
                scope.overStar = value;
                scope.rateDisplay = value;
            };

            //Post the new review
            scope.postReview = function (beer) {
                var review = {
                    _beer: beer._id,
                    comment: scope.review.comment,
                    rating: scope.review.rate
                };

                reviewService.postReview(review).then(function (results) {
                    //Reload reviews
                    scope.getReviews(review._beer);
                    scope.getBeers();
                    //Reinit fields
                    reinitializeReview();
                });

            };

            //Reinit form fields
            function reinitializeReview() {
                scope.review = {};
                scope.review.rate = 7;
            }

            reinitializeReview();
        }
    }
}])
;
