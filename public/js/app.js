'use strict';

angular.module('beerio.services', ['ui.bootstrap', 'restangular']);
angular.module('beerio.controllers', ['beerio.services', 'mwl.bluebird']);
angular.module('beerio.directives', ['beerio.controllers']);
angular.module('beerio.utils', ['angularMoment']);

var app = angular.module('beerio', ['beerio.controllers', 'beerio.services', 'beerio.directives', 'beerio.utils']);
