/**
 * Created by Komo on 29/03/2015.
 */

angular.module('beerio.services').factory('sessionService', ['Restangular', function(Restangular) {

    var sessionService = {};

    //Get session user
    sessionService.getSessionUser = function()
    {
        return Restangular.one('session').get();
    };

    return sessionService;
}]);

