/**
 * Created by Komo on 29/03/2015.
 */

var calc = function calc() {

    //Fonction retournant la somme des arguments
    function addArgs(args) {
        var sum = 0;
        for (var i = 0; i < args.length; i++) {
            sum += args[i]
        }
        return sum;
    }

    var result = addArgs(arguments);

    function mult() {
        result *= addArgs(arguments);
        return mult
    }

//Si mult est appelée dans un context String, on appelle cette              méthode à la place

    mult.toString = function() { return result};

    return mult;
};


module.exports.calc = calc;