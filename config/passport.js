var TwitterStrategy  = require('passport-twitter').Strategy;
var User = require('../models/user');
var configAuth = require('./auth');

module.exports = function(passport) {

    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use(new TwitterStrategy({

            consumerKey     : configAuth.twitterAuth.consumerKey,
            consumerSecret  : configAuth.twitterAuth.consumerSecret,
            callbackURL     : configAuth.twitterAuth.callbackURL

        },
        function(token, tokenSecret, profile, done) {

            //Async, waits for all infos
            process.nextTick(function() {

                User.findOne({oauthId : profile.id, provider: "twitter" }, function(err, user) {

                    if (err)
                        return done(err);

                    //If user exists, return that user
                    if (user) {
                        return done(null, user);
                    } else {
                        //User not found, creating
                        var newUser = new User();

                        newUser.oauthId = profile.id;
                        newUser.provider = "twitter";
                        newUser.token = token;
                        newUser.username = profile.username;
                        newUser.displayName = profile.displayName;
                        newUser.image = profile._json.profile_image_url;

                        //Save in DB
                        newUser.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, newUser);
                        });
                    }
                });

            });

        }));

};