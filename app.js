var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');

var session = require('express-session');

var RedisStore = require('connect-redis')(session);
var redis = require("redis").createClient();

var flash = require('connect-flash');
var passport = require('passport');
var Promise = require('bluebird');
var config = require('./config');
var mongoose = Promise.promisifyAll(require('mongoose'));

require('./config/passport')(passport);
var index = require('./routes/index');
var auth = require('./routes/auth');
var beers = require('./routes/beers');
var reviews = require('./routes/reviews');
var app = express();


// Database
console.log(config.db[app.settings.env]);
app.set('dbUrl', config.db[app.settings.env] || 'development');
mongoose.connect(app.get('dbUrl'), function(err) {
    if(err) {
        console.log('Connection error', err);
    } else {
        console.log('Connection successful');
    }
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(cookieParser());

//Redis pour la persistance de la session
app.use(session({
    secret: "S3CR3T0KENBEERIO",
    saveUninitialized: true,
    resave: true,
    store : new RedisStore({
        host : '127.0.0.1',
        port : '6379',
        user : 'redis-username',
        pass : ''
    }),
    cookie : {
        maxAge : 604800 // one week
    }
}));

app.use(passport.initialize());
app.use(passport.session()); // persistent login sessions
app.use(flash()); // use connect-flash for flash messages stored in session

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));
//Bower components access
app.use('/bower',  express.static(__dirname + '/bower_components'));

app.use('/', auth);
app.use('/', index);
app.use('/beers', beers);
app.use('/reviews', reviews);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});

module.exports = app;
