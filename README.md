How to run Beerio :

Prerequisites :

1) Have a mongodb server running
2) Have a redis server running

1) bower install
2) npm install
3) Edit file app.js (redis) and config.js (mongodb) and configure databases :

Redis :
These parameters are Redis default.
app.use(session({
    secret: "S3CR3T0KENBEERIO",
    saveUninitialized: true,
    resave: true,
    store : new RedisStore({
        host : '127.0.0.1',
        port : '6379',
        user : 'redis-username',
        pass : ''
    }),
    cookie : {
        maxAge : 604800 // one week
    }
}));

4) Run the server :
- npm start or node app.js
Visit URL 127.0.0.1/beerio


Launching tests :

1) Tests require a test database, configured in config.js.
At the moment data is also required in the test database :
 I suggest you play with the application a bit then
 run db.copyDatabase("beerio", "beeriotest", "127.0.0.1") (beerio = name of test db).
 Sorry for this temporary bad solution...


2) Launch the node server with the test parameters :
NODE_ENV=test npm start
Run in another terminal : npm test

Known bugs :

1) Twitter login might take two tries the first time : still investigating...